export default {
  addTodo (state, todo) {
    state.todos.push(todo)
    /* eslint-disable no-console */
    console.log(todo);
    console.log(state.todos);
    /* eslint-enable no-console */
  },
  changeTodo (state, payload) {
    state.todos[+payload.id] = payload.todo
  },
  removeTodo (state, id) {
    state.todos.splice(id,1)
  }
}