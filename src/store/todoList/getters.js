export default {
  todos: state => state.todos,
  getTodoById: (state) => (id) => {
    return state.todos[id]
  }
}