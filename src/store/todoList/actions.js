export default {
  saveTodo (context, payload) {
    if (payload.id === 'new') {
      context.commit('addTodo', payload.todo)
    } else {
      context.commit('changeTodo', payload)
    }
  },
  deleteTodo (context, id) {
    context.commit('removeTodo',id)
  }
}