import mutations from './mutations'
import actions from './actions'
import getters from './getters'

const todoList = {
  state: {
    todos: []
  },
  mutations,
  actions,
  getters
}

export default todoList