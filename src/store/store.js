import Vue from 'vue'
import Vuex from 'vuex'
//Save store in localstorage for future sessions
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex)

//Import and add TodoList module in store
import todoList from './todoList/store'

export default new Vuex.Store({
  modules: {
    todoList
  },
  plugins: [createPersistedState()],
})