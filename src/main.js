import Vue from 'vue'
import App from './App.vue'
import VueLodash from 'vue-lodash'
import VModal from 'vue-js-modal'

Vue.use(VModal)
Vue.use(VueLodash)

import styles from './scss/app.scss'
import router from './routes.js'
import store from './store/store'

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  styles,
  render: h => h(App)
})
