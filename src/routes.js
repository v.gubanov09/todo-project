import Vue from 'vue'
import VueRouter from 'vue-router'

//Import page components
import homePage from './views/homePage'
import singleTodo from './views/singleTodo'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    {
      path: '/',
      name: 'home',
      component: homePage
    },
    {
      path: '/:id',
      name: 'single',
      component: singleTodo
    },
    {
      path: '*',
      redirect: '/'
    }
  ],
  mode: 'history'
})

export default router
